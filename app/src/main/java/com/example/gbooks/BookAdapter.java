package com.example.gbooks;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class BookAdapter extends ArrayAdapter<Book> {

    public BookAdapter(Context context, List<Book> books )
    {
        super(context, 0, books);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Check if there is an existing list item view (called convertView) that we can reuse,
        // otherwise, if convertView is null, then inflate a new list item layout.
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.book_list_item, parent, false);
        }

        Book currentBook = getItem(position);


        TextView authorTextView = (TextView) listItemView.findViewById(R.id.author_text);
        authorTextView.setText(currentBook.getAuthor());

        TextView titleTextView = (TextView) listItemView.findViewById(R.id.title_text);
        titleTextView.setText(currentBook.getTitle());

        TextView subtitleTextView = (TextView) listItemView.findViewById(R.id.subtitle_text);
        if(!isNullString(currentBook.getSubtitle()))
        {
            subtitleTextView.setText(currentBook.getSubtitle());
        }
        else if(isNullString(currentBook.getSubtitle()))
        {
            subtitleTextView.setVisibility(View.GONE);
        }

            ImageView imgView = (ImageView)listItemView.findViewById(R.id.thumbnail);
            imgView.setImageBitmap(currentBook.getImg());


            TextView ratingTextView = (TextView) listItemView.findViewById(R.id.rating);
            ratingTextView.setText(String.valueOf(currentBook.getPageCount()));

        return listItemView;


    }

    private boolean isNullString(String s)
    {
        boolean b = false;
        if (s.equals("null") || (!TextUtils.equals(s ,"null")) || (!TextUtils.isEmpty(s)) || s.equals(null))
        {
            b = true;
        }
        return b;

    }


    /**
     * Return the color for the magnitude circle based on the intensity of the earthquake.
     *
     * @param magnitude of the earthquake
     */


}
