package com.example.gbooks;

/**
 * Created by Grey on 30-Jun-17.
 */


import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

public class BookLoader extends AsyncTaskLoader<List<Book>>{

    private static String url = "https://www.googleapis.com/books/v1/volumes?q=";
    private static String modifiedUrl = "https://www.googleapis.com/books/v1/volumes?q=";
    private static String startIndex = "&startIndex=";
    private String query;
    public BookLoader(Context context, String query)
    {
        super(context);
        this.query=query;
        //loadNextDataFromApi(this.query, 0);
    }
//    @Override
//    protected void onStartLoading() {
//        forceLoad();
//    }

    @Override
    public List<Book> loadInBackground() {

        if(url==null)
        {
            return null;
        }
        List<Book> books  = QueryUtils.fetchBookData(modifiedUrl);
        return books ;
    }

    public static void loadNextDataFromApi(String s, int offset) {
        offset *= 10;
        String p = offset + "";
        startIndex += p;
        modifiedUrl += s;
        modifiedUrl += startIndex;
        //url+=s;

        startIndex = "&startIndex=";

    }

}
