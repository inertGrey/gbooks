package com.example.gbooks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Grey on 26-Jun-17.
 */

public class Book {

    private String title, subtitle, author, descp, avgRating;
    private int pageCount, ratingCount;
    private Bitmap img;



    public Book(String title, String subtitle, String author, Bitmap img, int pageCount)//, String descp, float avgRating, int pageCount, int ratingCount) {
    {   this.title = title;
        this.subtitle = subtitle;
        this.author = author;
        this.img=img;


        this.descp = descp;
        this.avgRating = avgRating;
        //avgRatingf = Float.parseFloat(avgRatingS);
        this.pageCount = pageCount;
//        this.ratingCount = ratingCount;
    }


    public Bitmap getImg() {
        return img;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescp() {
        return descp;
    }

//    public String getAvgRating() {
//        return avgRating;
//    }

    public int getPageCount() {
        return pageCount;
    }

    public int getRatingCount() {
        return ratingCount;
    }
}
