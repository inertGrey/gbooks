package com.example.gbooks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Grey on 26-Jun-17.
 */

public class QueryUtils {

    /** Tag for the log messages */
    public static final String LOG_TAG = QueryUtils.class.getSimpleName();

    //private static final String SEARCH_QUERY = "https://www.googleapis.com/books/v1/volumes?q=";


    /**
     * Create a private constructor because no one should ever create a {@link QueryUtils} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */
    private QueryUtils() {
    }

    public static List<Book> fetchBookData(String requestUrl)
    {
        URL url = createURL(requestUrl);

        // Perform HTTP request to the URL and receive a JSON response back
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }

        // Extract relevant fields from the JSON response and create an {@link Earthquake} object


        // Return the {@link Event}
        return extractFeatureFromJson(jsonResponse);

    }

    private static List<Book> extractFeatureFromJson(String jsonResponse)
    {
        // If the JSON string is empty or null, then return early.
        if (TextUtils.isEmpty(jsonResponse)) {
            return null;
        }

        // Create an empty ArrayList that we can start adding earthquakes to
        List<Book> books = new ArrayList<>();

        try{
            JSONObject baseJsonResponse = new JSONObject(jsonResponse);
            JSONArray booksArray = baseJsonResponse .getJSONArray("items");


            String title = "null";
            String subtitle = "null";
            //String descp = "null";
            String author="null";
            String imgUrl = "null";
            Bitmap img = null;
            int pageCount = 0;
            String pCount;


            for(int i=0; i <booksArray.length(); i++)
            {
                try{
                    JSONObject currentBook = booksArray.getJSONObject(i);

                    JSONObject volInfo=currentBook.getJSONObject("volumeInfo");
                    title = volInfo.getString("title");


                    JSONArray authorArray = volInfo.getJSONArray("authors");
                    author = authorArray.getString(0);

                    JSONObject imageLinks = volInfo.getJSONObject("imageLinks");

                    imgUrl = imageLinks.getString("thumbnail");

                    img = assignBitmap(imgUrl);

                    pageCount = volInfo.getInt("pageCount");




                    //descp = volInfo.getString("description");


                    subtitle = volInfo.getString("subtitle");


                }
                catch (Exception e)
                {

                }
                books.add(new Book(title, subtitle, author, img, pageCount));


                //Log.e("query utils", authorJ);


            }

        }catch(Exception e)
        {
            Log.e("QueryUtils", "Problem parsing the books JSON results", e);

        }



        return  books;


    }

    private static Bitmap assignBitmap(String url)
    {
        Bitmap img = null;
        try
        {
            InputStream in = new java.net.URL(url).openStream();
            img = BitmapFactory.decodeStream(in);
        }catch(IOException e)
        {
            Log.e("main", "doInBackground");
            e.printStackTrace();
        }
        return img;
    }

    private static String makeHttpRequest(URL url) throws IOException
    {
        String jsonResponse="";
        if(url==null)
        {return jsonResponse;}

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //if the request was successful (response code 200)
            //then read the input stream and parse the response
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code" + urlConnection.getResponseCode());
            }
        }catch (IOException e)
        {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException
    {

        StringBuilder output = new StringBuilder();
        if(inputStream!=null)
        {
            InputStreamReader isr = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader br = new BufferedReader(isr);
            String line = br.readLine();

            while(line!=null)
            {
                output.append(line);
                line=br.readLine();
            }

        }
        return output.toString();
    }


    private static URL createURL(String stringUrl)
    {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException exception) {
            Log.e(LOG_TAG, "Error with creating URL", exception);
            return null;
        }
        return url;
    }



}
